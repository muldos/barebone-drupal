# Barebone Drupal 8
My drupal 8 stack for big projects.

## Features
* PHP 7.3 / MySQL 8
* Elasticsearch and kibana integration
* BigPipe
* Advanced caching using memcached
* config split / config ignore
* ultimate cron
* CI / CD
* Dockerized
* core patches for blocks, views and layout builder
* patches for elasticsearch : more like this and webprofiler compatibility

## Vscode and linux
Made for vscode and linux

## Unit test 
to launch unit test suite : 
This project uses the drupal-template so composer.json is not at the expected location of the composer unit tests' group.
To launch core unit tests without error :

<code>cd web/core;../../vendor/bin/phpunit --testsuit=unit --exclude-group=Composer</code>